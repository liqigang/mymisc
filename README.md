
## 负二项分布
原理和R代码例子
http://www.karlin.mff.cuni.cz/~pesta/NMFM404/NB.html

## beta分布及共轭Bernoulli分布-先验、后验、预测分布
通俗易懂,马同学公众号文章,如何理解贝叶斯推断和beta分布

公式推导 https://blog.csdn.net/qy20115549/article/details/53307535

## Q-value
https://www.statisticshowto.datasciencecentral.com/q-value/

## A simple variant calling pipeline step by step
https://gencore.bio.nyu.edu/variant-calling-pipeline/
